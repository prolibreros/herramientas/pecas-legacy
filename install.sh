#!/bin/bash

# Descarga e instala pecas. Permite que la usuaria especifique la ubicación de
# instalación por medio de la variable PECAS_HOME
PECAS_HOME=${PECAS_HOME:-$HOME/.local/opt/pecas}
mkdir -p $PECAS_HOME
git clone --depth 1 https://gitlab.com/programando-libreros/herramientas/pecas-legacy.git $PECAS_HOME

# Menciona dónde fue descargado Pecas
echo "----------------------------------"
echo "=> Pecas se ha descargado en '$PECAS_HOME'."

# Sustituye los espacios en la ruta
space="\\ "
PECAS_HOME=${PECAS_HOME// /$space}

# Según el tipo de intérprete, es el archivo de perfil
current_shell=$(ps -p $$)
if [[ "$current_shell" =~ "zsh" ]]; then
  profile_script=~/.zprofile
elif [[ "$current_shell" =~ "bash" ]]; then
  profile_script=~/.profile
elif [[ "$current_shell" =~ "sh" ]]; then
  profile_script=~/.profile
else
  profile_script=~/.profile
  echo "=> ! No se pudo detectar el intérprete de tu terminal, quizás tengas que renombrar el archivo '$profile_script' para que sea cargado al iniciar tu terminal."
fi

# Según si el archivo de perfil existe o no
if [ ! -f "$profile_script" ]; then
  echo "=> ! No se encontró archivo de configuración, creando archivo '$profile_script'."
fi
echo "=> Añadiendo variables de entorno para Pecas (binarios, manual y EPUBCheck) en '$profile_script'."
touch $profile_script

# La condición también viene de https://github.com/NikaZhenya/sexy-bash-prompt/blob/master/install.bash
if ! grep PECAS_HOME "$profile_script" &> /dev/null; then
    echo "" >> $profile_script
    echo "# Pecas" >> $profile_script
    echo "export PECAS_HOME=$PECAS_HOME" >> $profile_script
    echo "export PATH=\$PATH:\$PECAS_HOME/bin" >> $profile_script
    echo "" >> $profile_script
    echo "# Pecas (manual)" >> $profile_script
    echo "export PATH=\$PATH:\$PECAS_HOME/docs/man" >> $profile_script
    echo "" >> $profile_script
    echo "# EpubCheck" >> $profile_script
    echo "export PATH=\$PATH:\$PECAS_HOME/src/alien/epubcheck/bin" >> $profile_script
    echo "" >> $profile_script
else
    echo "=> Al parecer ya se han agregado las herramientas de Pecas a '$profile_script'."
fi

# Fin, lo ideal es que fuera automático
echo "=> Para comenzar a usar Pecas usa 'source $profile_script' (sin las comillas) o abre una nueva terminal."
echo "=> Usa 'pc-doctor' (sin las comillas) para ver el estado de Pecas y sus dependencias."
